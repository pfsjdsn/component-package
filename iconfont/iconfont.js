Component({
  properties: {
    // icondayuhao | iconweather0 | icontianqitubiao-xiayu | iconduoyun | iconziyuan | iconsangedian | icondengdai1 | iconcuowu | iconchenggong | iconxinxi | iconjinggao | icontixianchenggong | iconshouru | icontixianzhong | iconwechat | iconduigou-copy | icondengdai | iconic_flow_key_star | iconxuhao2 | iconxuhao3 | iconxuhao4 | iconxuhao21 | iconxuhao | iconxuhao1 | iconnongyishi | iconqueshaotupianicon | iconqueshaoxuke | iconnongchangzhu1 | iconhuabanfuben | iconrenzhengyiguojiyuan | iconweixin2 | iconguojiyuan | iconguanbi | iconxitongxiaoxi1 | iconicon_sousuo | iconshouyedingweitubiao | iconguanbidanchuang | iconxuanzhong | iconweixuanzhong | iconzhangdan | icongerenyinsi | iconduigou | icondailishenqing | iconbitian | iconsaomiaorenxiangtubiao | iconshanchutubiao2x | iconwodec | iconwode | iconxiaoxic | iconxiaoxi | iconqiuzhi | iconqiuzhia | iconicon_tianqiqing | icondaosanjiao | iconbiaoqing | iconfanhui | icongengduo | iconfenxiang | iconjianyi | icondianhualianxi | iconchangyongyu | icongengduojiahao | iconmorentouxiang | iconlishijilu | iconnan | iconnandefuben | iconnvdefuben | iconkefu | iconnv | iconrenzhengnongchangzhu | iconnongchangzhu | iconqianjin | iconpaizhao | iconpengyouquan | iconrenzhengshejishi | iconshengcheng | iconshangchuanzhaopian | iconrenzhengyirenzheng | icontixing | iconweixin | iconweirenzheng | iconweibo | iconwodefuwu | iconyirenzheng | iconwoderenzheng | iconicon_weixin | iconyuyin | iconicon_shanchu | iconqq | iconshancuanniu1 | iconicon_dingweizuobiao | iconshancuanniu
    name: {
      type: String,
    },
    // string | string[]
    color: {
      type: null,
      observer: function(color) {
        this.setData({
          colors: this.fixColor(),
          isStr: typeof color === 'string',
        });
      }
    },
    size: {
      type: Number,
      value: 18,
      observer: function(size) {
        this.setData({
          svgSize: size,
        });
      },
    },
  },
  data: {
    colors: '',
    svgSize: 18,
    quot: '"',
    isStr: true,
  },
  methods: {
    fixColor: function() {
      var color = this.data.color;
      var hex2rgb = this.hex2rgb;

      if (typeof color === 'string') {
        return color.indexOf('#') === 0 ? hex2rgb(color) : color;
      }

      return color.map(function (item) {
        return item.indexOf('#') === 0 ? hex2rgb(item) : item;
      });
    },
    hex2rgb: function(hex) {
      var rgb = [];

      hex = hex.substr(1);

      if (hex.length === 3) {
        hex = hex.replace(/(.)/g, '$1$1');
      }

      hex.replace(/../g, function(color) {
        rgb.push(parseInt(color, 0x10));
        return color;
      });

      return 'rgb(' + rgb.join(',') + ')';
    }
  }
});
