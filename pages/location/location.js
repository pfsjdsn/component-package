let allCity = require('../../utils/allcity.js');
Page({
    /**
     * 页面的初始数据
     */
    data: {
        allCity: [], // 城市数据
        rightLetter: [], // 右侧字母展示
        jumpNum: "", // 跳转到哪个字母
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.showLoading({
            title: '加载数据中...',
        })
        // 模拟服务器请求异步加载数据
        setTimeout(() => {
            this.setData({
                allCity
            }, () => {
                this.getRightLetter()
            })
            wx.hideLoading()
        }, 2000)
    },
    // 获取城市和字母的数据
    getRightLetter() {
        let { allCity, rightLetter } = this.data
        rightLetter = allCity.map(item => item.title)
        this.setData({
            rightLetter
        })
    },
    // 点击右侧的字母，滚动到对应的数据中
    handleScroll(e) {
        const jumpNum = e.target.dataset.index
        this.setData({
            jumpNum
        })
    }
})