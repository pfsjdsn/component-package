// pages/area/area.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    areaFlag: -1,
    cityList: [
      {
        id: 0,
        city: '南昌市',
        child: [
          { id: 0,
            district: '南昌县'
          },
          { id: 1,
            district: '新建区'
          },
          { id: 2,
            district: '安义县'
          },
          { id: 3,
            district: '进贤县'
          },
          { id: 4,
            district: '莲塘镇'
          }
        ]
      },
      {
        id: 1,
        city: '景德镇市',
        child: [
          { id: 0,
            district: '乐平市'
          },
          { id: 1,
            district: '浮梁县'
          },
          { id: 2,
            district: '景德镇市'
          },
        ]
      },
      {
        id: 2,
        city: '萍乡市',
        child: [
          {
            id: 0,
            district: '萍乡市'
          },
          {
            id: 1,
            district: '莲花县'
          },
          {
            id: 2,
            district: '上栗县'
          },
          {
            id: 3,
            district: '芦溪县'
          },
          {
            id: 4,
            district: '湘东区'
          },
          {
            id: 5,
            district: '安源区'
          }
        ]
      },
      {
        id: 3,
        city: '九江市',
        child: [
          {
            id: 0,
            district: '九江市'
          },
          {
            id: 1,
            district: '武宁县'
          },
          {
            id: 2,
            district: '修水县'
          },{
            id: 3,
            district: '永修县'
          },
          {
            id: 4,
            district: '德安县'
          },
          {
            id: 5,
            district: '庐山市'
          },
          {
            id: 6,
            district: '都昌县'
          },
          {
            id: 7,
            district: '湖口县'
          },{
            id: 8,
            district: '彭泽县'
          },
          {
            id: 9,
            district: '濂溪区'
          },
          {
            id: 10,
            district: '瑞昌市'
          }


        ]
      },
      {
        id: 4,
        city: '新余市',
        child: [
          {
            id: 0,
            district: '新余市'
          },
          {
            id: 1,
            district: '分宜县'
          }
        ]
      },
      {
        id: 5,
        city: '鹰潭市',
        child: [
          {
            id: 0,
            district: '鹰潭市'
          },
          {
            id: 1,
            district: '贵溪市'
          },
          {
            id: 2,
            district: '余江区'
          }
        ]
      },
      {
        id: 6,
        city: '赣州市',
        child: [
          {
            id: 0,
            district: '赣州市'
          },
          {
            id: 1,
            district: '瑞金市'
          },{
            id: 2,
            district: '南康区'
          },
          {
            id: 3,
            district: '赣县'
          },
          {
            id: 4,
            district: '信丰县'
          },
          {
            id: 5,
            district: '大余县'
          },
          {
            id: 6,
            district: '上犹县'
          },{
            id: 7,
            district: '崇义县'
          },
          {
            id: 8,
            district: '安远县'
          },
          {
            id: 9,
            district: '龙南县'
          },
          {
            id: 10,
            district: '定南县'
          },
          {
            id: 11,
            district: '全南县'
          },{
            id: 12,
            district: '宁都县'
          },
          {
            id: 13,
            district: '于都县'
          },
          {
            id: 14,
            district: '兴国县'
          },
          {
            id: 15,
            district: '会昌县'
          },{
            id: 16,
            district: '寻乌县'
          },
          {
            id: 17,
            district: '石城县'
          }
        ]
      },
      {
        id: 7,
        city: '吉安市',
        child: [
          {
            id: 0,
            district: '吉安市'
          },
          {
            id: 1,
            district: '吉水县'
          },
          {
            id: 2,
            district: '峡江县'
          },
          {
            id: 3,
            district: '新干县'
          },
          {
            id: 4,
            district: '永丰县'
          },
          {
            id: 5,
            district: '遂川县'
          },
          {
            id: 6,
            district: '泰和县'
          },
          {
            id: 7,
            district: '万安县'
          },
          {
            id: 8,
            district: '安福县'
          },
          {
            id: 9,
            district: '永新县'
          },
          {
            id: 10,
            district: '宁冈县'
          },
          {
            id: 11,
            district: '井冈山市'
          },
          {
            id: 12,
            district: '吉安县'
          }


        ]
      },
      {
        id: 8,
        city: '宜春市',
        child: [
          {
            id: 0,
            district: '宜春市'
          },
          {
            id: 1,
            district: '丰城市'
          },
          {
            id: 2,
            district: '樟树市'
          },
          {
            id: 3,
            district: '高安市'
          },
          {
            id: 4,
            district: '奉新县'
          },
          {
            id: 5,
            district: '万载县'
          },
          {
            id: 6,
            district: '上高县'
          },
          {
            id: 7,
            district: '宜丰县'
          },
          {
            id: 8,
            district: '靖安县'
          },
          {
            id: 9,
            district: '铜鼓县'
          }
        ]
      },
      {
        id: 9,
        city: '抚州市',
        child: [
          {
            id: 0,
            district: '抚州市'
          },
          {
            id: 1,
            district: '南城县'
          },
          {
            id: 2,
            district: '黎川县'
          },
          {
            id: 3,
            district: '南丰县'
          },
          {
            id: 4,
            district: '崇仁县'
          },
          {
            id: 5,
            district: '乐安县'
          },
          {
            id: 6,
            district: '宜黄县'
          },
          {
            id: 7,
            district: '金溪县'
          },
          {
            id: 8,
            district: '资溪县'
          },
          {
            id: 9,
            district: '东乡县'
          },
          {
            id: 10,
            district: '广昌县'
          }
        ]
      },
      {
        id: 10,
        city: '上饶市',
        child: [
          {
            id: 0,
            district: '上饶市'
          },
          {
            id: 1,
            district: '德兴市'
          },
          {
            id: 2,
            district: '黎川县'
          },
          {
            id: 3,
            district: '上饶县'
          },
          {
            id: 4,
            district: '广丰区'
          },
          {
            id: 5,
            district: '玉山县'
          },
          {
            id: 6,
            district: '铅山县'
          },
          {
            id: 7,
            district: '横峰县'
          },
          {
            id: 8,
            district: '弋阳县'
          },
          {
            id: 9,
            district: '余干县'
          },
          {
            id: 10,
            district: '波阳'
          },
          {
            id: 11,
            district: '万年县'
          },
          {
            id: 12,
            district: '婺源县'
          },
          {
            id: 13,
            district: '鄱阳县'
          }
        ]
      }
    ],
    province: '江西省',
    city: '',
    district: '',
    flag1: -1,
    flag2: -1
  },
  selectCity: function(e) {
    const that = this
      that.setData({
        flag1: e.currentTarget.dataset.id
      })
      that.setData({
        city: e.currentTarget.dataset.text
      })
  },
  selectDistrict: function (e) {
    const that = this
    that.setData({
        flag2: e.currentTarget.dataset.id
      })
      that.setData({
        district: e.currentTarget.dataset.text
      })
  },
  define:function () {
    console.log('确定...')

  },
  reset:function () {
    const that = this
    console.log('重置...')
    that.setData({
      flag1: -1,
      flag2: -1,
      province: '',
      city: '',
      district: ''
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    that.setData({
      areaFlag: options.title
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})