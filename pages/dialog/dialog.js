// pages/dialog/dialog.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        closeOnClickModal: false, // 是否可点击空白处关闭遮罩层
        dialogvisible1: false,
        inputs1: [{
            text: '弹窗标题：',
            value: '弹窗标题！',
        },
        {
            text: '弹窗宽度：',
            value: '85%',
        },
    ],
    switchs1: [{
            text: '开启动画',
            value: true,
        },
        {
            text: '是否可以点击modal关闭Dialog',
            value: false,
        },
        {
            text: '确认按钮是否带 loading 图标',
            value: false,
        },
        {
            text: '是否收集formId',
            value: false,
        },
    ],
    buttonConf1: [{
            title: '确认按钮',
            text: '同意',
            show: false,
            color: '#fff',
            background: '#0486FE',
            openType: '',
        },
        {
            title: '取消按钮',
            text: '不同意',
            show: false,
            color: '#999999',
            background: '#ffffff',
            openType: '',
        },
    ],
    },
    showDialog1: function () {
		const that =  this
		that.setData({
			dialogvisible1: true,
		})
	},
	closeDialog1: function () {
		const that =  this
		that.setData({
			dialogvisible1: false,
		})
	},
	confirm1: function () {
		const that =  this
		that._toast('confirm')
	},
	cancel1: function () {
		const that =  this
		that.closeDialog1()
    },
    
    // 
    _toast(msg) {
		wx.showToast({
			title: msg,
			icon: 'none',
			duration: 2000,
		})
    },
    click: function () {
        this.setData({
            dialogvisible1: true
        })
    },
    disagree: function () {
        this.setData({
            dialogvisible1: false
        })
    },
    agree: function () {
        this.setData({
            dialogvisible1: false
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})