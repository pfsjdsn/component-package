// pages/upload/upload.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        imgs: [],
        userName:'demo'
    },
    // 提交图片
    submit: function () {
        const that = this
        wx.uploadFile({
			url: 'http://192.168.1.10:19001/aa/bb', // 接口地址
			filePath: that.data.tempFilePaths[0], // 需要传入的值（存放图片的数组）
			name: 'photoes', // 参数的名字
			formData: {
				userName: that.data.userName, // 其它需要传给接口的参数
				
			},
			header: {
				"Content-Type": "multipart/form-data",
				'access_token': wx.getStorageSync('token')
			},
			success(res) {
                console.log('提交成功！')
			},
			fail(error) {
				console.log(error);
			}
		})
        
    },
    	// 上传图片
	chooseImg: function (e) {
		var that = this;
		var imgs = that.data.imgs;
		if (imgs.length >= 9) { // 限制可上传张数
			that.setData({
				lenMore: 1
			});
			setTimeout(function () {
				that.setData({
					lenMore: 0
				});
			}, 2500);
			return false;
		}
		wx.chooseImage({
			// count: 1, // 默认9
			sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
			sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
			success: function (res) {
				// 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
				// var tempFilePaths = res.tempFilePaths
				that.setData({
					tempFilePaths: res.tempFilePaths
				})

				var imgs = that.data.imgs
				for (var i = 0; i < that.data.tempFilePaths.length; i++) {
					if (imgs.length >= 9) {
						that.setData({
							imgs: imgs
						});
						return false
					} else {
						imgs.push(that.data.tempFilePaths[i])
					}
				}
				that.setData({
					imgs: imgs
				})
				console.log(imgs)
			}
		})
	},
	// 删除图片
	deleteImg: function (e) {
		const that = this
		var imgs = that.data.imgs;
		var index = e.currentTarget.dataset.index;
		imgs.splice(index, 1);
		that.setData({
			imgs: imgs
		});
	},
	// 预览图片
	previewImg: function (e) {
		const that = this
		//获取当前图片的下标
		var index = e.currentTarget.dataset.index;
		//所有图片
		var imgs = that.data.imgs;
		wx.previewImage({
			//当前显示图片
			current: imgs[index],
			//所有图片
			urls: imgs
		})
	},
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})