### 1、iconfont彩色图标的使用

- 第一步：安装插件

  ```vue
  // Yarn
  yarn add mini-program-iconfont-cli --dev
  // Npm
  npm install mini-program-iconfont-cli --save-dev
  ```

- 第二步：生成配置文件

  ```vue
  npx iconfont-init
   
  // 或者
   
  npx iconfont-wechat
  ```

- 第三步
  symbol_url --- 复制iconfont官网你的项目的Symbol链接

- 第四步：生成小程序组件

  ```vue
  npx iconfont-wechat
  ```

- 第五步：注册iconfont组件并使用

  ```vue
  app.json文件
  
  // 绝对路径
  {
      "usingComponents": {
          "iconfont": "/iconfont/iconfont"
      }
  }
  ```

  

- 使用

  ```VUE
  <iconfont name="iconweather0"></iconfont>
  ```

- 更新iconfont项目中的图标后

  ```VUE
  npx iconfont-wechat
  ```

  